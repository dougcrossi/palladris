namespace Shared

open System.Collections.Generic

module Route =
    let builder = sprintf "/api/%s/%s"

[<CLIMutable>]
type PairPrice =
    {
        Id: int
        Date: System.DateTime
        Pair: string
        Price: float
        Quantity: int
    }

[<CLIMutable>]
type PriceChart =
    {
        Id: int
        Date: System.DateTime
        Price: float
    }

type PalladrisApi =
    { GetPairPrice : Async<List<PairPrice>>
      AddPairPrice : PairPrice -> Async<PairPrice>
      GetPriceChart : Async<List<PriceChart>>}
