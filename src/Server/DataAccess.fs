module DataAccess

open Shared

open System.IO
open Microsoft.EntityFrameworkCore
open Microsoft.Extensions.Configuration

let configurationRoot = 
    let builder = (new ConfigurationBuilder())
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", true, true)
                    .AddEnvironmentVariables()
    builder.Build()

type MyContext =
    inherit DbContext

    [<DefaultValue>]
    val mutable PairPrice : DbSet<PairPrice>
    [<DefaultValue>]
    val mutable PriceChart : DbSet<PriceChart>

    member public this._PairPrice      with    get()   = this.PairPrice 
                                       and     set value  = this.PairPrice <- value 
                                      
    member public this._PriceChart     with    get()   = this.PriceChart 
                                       and     set value  = this.PriceChart <- value 
    
    new() = { inherit DbContext() }
    
    override __.OnConfiguring optionsBuilder =
        let connectionString = configurationRoot.GetValue<string>("connectionString")
        let dbtype = configurationRoot.GetValue<string>("dbtype")
        if optionsBuilder.IsConfigured <> true then
            //Add here more database connections and change dbtype in appsettings.json
            if dbtype = "postgres" then
                //Install Npgsql.EntityFrameworkCore.PostgreSQL package
                optionsBuilder.UseNpgsql(connectionString)|> ignore
            (*if dbtype = "inmemory" then
                //Install Microsoft.EntityFrameworkCore.InMemory
                optionsBuilder.UseInMemoryDatabase("palladris") |> ignore*)

    member this.Initialize = 
        this.Database.EnsureCreated() |> ignore //check if the database is created, if not then creates it