module Api

open DataAccess
open Shared
open System.Linq

let ctx = new MyContext()
ctx.Initialize

let getPairPrice = async {
    return ctx.PairPrice.ToList<PairPrice>()
}

let addPairPrice (pairPrice: PairPrice) = async {
    printf "%s" pairPrice.Pair
    ctx.PairPrice.Add(pairPrice) |> ignore
    return pairPrice
}

let getPriceChart = async {
    return ctx.PriceChart.ToList<PriceChart>()
}

let PalladrisApi =
    { GetPairPrice = getPairPrice

      AddPairPrice = addPairPrice

      GetPriceChart = getPriceChart

    }