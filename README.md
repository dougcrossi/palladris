# Palladris

## Install pre-requisites
You'll need to install the following pre-requisites in order to build this applications

* The [.NET Core SDK](https://www.microsoft.com/net/download) 3.1
* The [.NET Core SDK](https://www.microsoft.com/net/download) higher
* [npm](https://nodejs.org/en/download/) package manager.
* [Node LTS](https://nodejs.org/en/download/).
* [PostgreSQL](https://www.postgresql.org/download/linux/ubuntu/).

The following commands might help, but may become outdated soon

***.NET Core SDK 3.1 and higher***
```bash
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-3.1

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-5.0
```

***Node and npm***
```bash
# Using Ubuntu
curl -fsSL https://deb.nodesource.com/setup_15.x | sudo -E bash -
sudo apt-get install -y nodejs
```

***PostgreSQL***
```bash
# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update the package lists:
sudo apt-get update

# Install the latest version of PostgreSQL.
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql
```

## Configuring DB and AppSettings.json
For security reasons the default password for the user ***postgres*** generated when installing ***PostgreSQL*** must be changed.  

```bash
sudo -u postgres psql
\password
    <AddNewPassword>
\q
```

The ***appsettings.json*** file is also not stored on the git repo to preserve the DB password. Under the folder ***src/Server/*** create a new file ***appsettings.json*** and add the following by replacing the new password.  

```json
{
    "connectionString": "Host=localhost;Database=palladris;Username=postgres;Password=<AddNewPassword>",
    "dbtype": "postgres"
}
```

## Starting the application
Start the server:
```bash
cd src\Server\
dotnet restore
dotnet run
```

Start the client (OPTIONAL):
```bash
npm install
npm run start
```

Open a browser to `http://localhost:8080` to view the site.

## SAFE Template
This template can be used to generate a full-stack web application using the [SAFE Stack](https://safe-stack.github.io/). It was created using the dotnet [SAFE Template](https://safe-stack.github.io/docs/template-overview/). If you want to learn more about the template why not start with the [quick start](https://safe-stack.github.io/docs/quickstart/) guide?

If you want to know more about the full Azure Stack and all of its components (including Azure) visit the official [SAFE documentation](https://safe-stack.github.io/docs/).

You will find more documentation about the used F# components at the following places:

* [Saturn](https://saturnframework.org/docs/)
* [Fable](https://fable.io/docs/)
* [Elmish](https://elmish.github.io/elmish/)
